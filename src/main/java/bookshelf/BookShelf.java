package bookshelf;
import java.util.List;

/**
 * This class represents a book shelf which is capable to store Book objects
 * in a certain order. The book shelf has a limited capacity.
 * @author Sondre Bolland and Martin Vatshelle
 *
 */
public class BookShelf {

	/* List of books in the bookshelf */
	public List<Book> books;
	/* Number of books there is space for on the bookshelf */
	private int capacity;
	
	/**
	 * Constructs a new empty BookShelf object with a given capacty
	 */
	public BookShelf(int capacity) {
		this.capacity = capacity;
	}
	
	/**
	 * Number of books currently in the bookshelf
	 * @return number of elements in <code>books</code>
	 */
	public int size() {
		return capacity;
	}
	
	/**
	 * Add book to bookshelf.
	 * @param book to add to bookshelf
	 * @throws IllegalArgumentException if the capacity of the
	 *  bookshelf is met, i.e. there is no room for more books
	 */
	public void addBook(Book book) {
		books.add(book);
	}
	
	/**
	 * Check if copy of <code>book</code> is in the bookshelf.
	 * @param book
	 * @return true if a copy of the book is in the bookshelf, false if not
	 */
	public boolean hasBook(Book book) {
		for (Book otherBook: books) {
			if (book == otherBook) {
				return true;
			}
		}
		return false;
	}
}



